#reCache
    ThinkPHP 使用方式
        第一种使用方式：
        vendor('Redis.StringCache');
        $ret = new \StringCache();
        $ret->addPre("rcs_");
        $condition["Id"]=array("IN",array(12,42));
        if($ret->is_cache($condition)){
            var_dump($ret->get_cache($condition));
        }else{
            $M=D("Manage/User");
            $res = $M->mfnSelect($condition);
            $ret->set_cache($condition,$res);
            var_dump($res);
        }

        第二种使用方式
        解决分页缓存，更改信息后可能出现的脏数据问题。
        暂时还有很多问题，这里只提供一种思路。
        vendor('Redis.StringCache');
        $ret = new \StringCache();
        $ret->addPre("rcs_");
        $condition["Id"]=array("IN",array(2,42));
        //查询缓存的ID数组
        if($ret->is_cache($condition)){
            //获取缓存的ID
            $arr_id=$ret->get_cache($condition);
        }else{
            $res=array(2,42);
            $ret->set_cache($condition,$res);
            $arr_id=$res;
        }

        //根据缓存的ID 从缓存中命中缓存
        $M=D("Manage/User");
        $ret->addPre("id_");
        $data=array();
        foreach($arr_id as $i => $v){
            if($ret->is_cache($v)){
                //获取缓存的ID
                $data[]=$ret->get_cache($v);
            }else{
                $res = $M->mfnFind("Id = ".$v);
                //缓存时效 这里应该给予较大值 每当修改本条数据时 将缓存作废
                $ret->set_cache($v,$res);
                $data[]=$res;
            }
        }

        var_dump($data);