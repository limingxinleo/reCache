<?php
// +----------------------------------------------------------------------
// | Demo [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.lmx0536.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: limx <715557344@qq.com> <http://www.lmx0536.cn>
// +----------------------------------------------------------------------
// | Date: 2016/7/15 Time: 16:15
// +----------------------------------------------------------------------
class SRedis
{
    private static $_instance = [];
    private static $_config = [
        'host' => '127.0.0.1',
        'port' => 6379
    ];

    public static function getInstance($config = [])
    {
        if (!empty($config)) {
            self::$_config = $config;
        }
        $id = md5(serialize(self::$_config));//获取自定义 实例号
        if (!isset(self::$_instance[$id])) {
            $instance = new Redis();
            $ret = $instance->connect(self::$_config['host'], self::$_config['port']);

            if ($ret === false) {
                die($instance->getLastError());
            }
            self::$_instance[$id] = $instance;
            //self::$_instance[$id] = new self();
        }

        return self::$_instance[$id];
    }

    private function __construct()
    {
        $instance = new Redis();
        $ret = $instance->connect(self::$_config['host'], self::$_config['port']);
        if ($ret === false) {
            die($instance->getLastError());
        }
        return $instance;
    }

    private function __clone()
    {
    }//覆盖__clone()方法，禁止克隆

    //魔术方法
    public function __call($name, $arguments)
    {
        return call_user_func_array([self::getInstance(), $name], $arguments);
    }

    // 调用驱动类的方法
    public static function __callStatic($method, $params)
    {
        // 自动初始化数据库
        return call_user_func_array([self::getInstance(), $method], $params);
    }

}
