<?php
// +----------------------------------------------------------------------
// | Demo [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.lmx0536.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: limx <715557344@qq.com> <http://www.lmx0536.cn>
// +----------------------------------------------------------------------
// | Date: 2016/5/15 Time: 0:05
// +----------------------------------------------------------------------
class BasicRedis
{
    protected $host = '127.0.0.1';
    protected $port = '6379';
    protected $handle;
    protected $pre = 're_';

    public function __construct($host = '127.0.0.1', $port = '6379', $pre = 're_')
    {
        if (isset($host)) {
            $this->host = $host;
        }
        if (isset($port)) {
            $this->port = $port;
        }
        if (isset($pre)) {
            $this->pre = $pre;
        }

        $this->handle = new Redis();
        $ret = $this->handle->connect($this->host, $this->port);
        if ($ret === false) {
            die($this->handle->getLastError());
        }
    }

    /**
     * [setPre 设置前缀]
     * @author limx
     * @param $key
     */
    public function setPre($key)
    {
        $this->pre = $key;
    }

    /**
     * [appPre 追加前缀]
     * @author limx
     * @param $key
     */
    public function addPre($key)
    {
        $this->pre .= $key;
    }

    public function keys($key)
    {
        return $this->handle->keys($this->pre . $key);
    }

    public function del($key)
    {
        return $this->handle->del($this->pre . $key);
    }

    public function close()
    {
        $this->handle->close();
    }

    //String操作相关 S

    /**
     * [set 字符串写]
     * @author limx
     * @param $key
     * @param $value
     * @param int $expire
     * @return mixed
     */
    public function set($key, $value, $expire = 0)
    {
        if ($expire == 0) {
            $ret = $this->handle->set($this->pre . $key, $value);
        } else {
            $ret = $this->handle->setex($this->pre . $key, $expire, $value);
        }
        return $ret;
    }

    /**
     * [get 字符串读]
     * @author limx
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        $func = is_array($key) ? 'mGet' : 'get';
        return $this->handle->{$func}($this->pre . $key);
    }
    //String操作相关 E


    //SET操作相关 S

    /**
     * [sAdd 加入集合新的元素]
     * @author limx
     * @param $key
     * @param $value
     * @return int
     */
    public function sAdd($key, $value)
    {
        return $this->handle->sAdd($this->pre . $key, $value);
    }

    /**
     * [sRemove 从集合中删除某个元素]
     * @author limx
     * @param $key
     * @param $value
     */
    public function sRemove($key, $value)
    {
        return $this->handle->sRemove($this->pre . $key, $value);
    }

    /**
     * [sToggle 从集合中添加某个值，存在值则删除]
     * @author limx
     * @param $key
     * @param $value
     * @return int|void
     */
    public function sToggle($key, $value)
    {
        if ($this->sContains($this->pre . $key, $value)) {
            return $this->sRemove($this->pre . $key, $value);
        } else {
            return $this->sAdd($this->pre . $key, $value);
        }

    }

    /**
     * [sContains 集合中是否存在某值]
     * @author limx
     * @param $key
     * @param $value
     */
    public function sContains($key, $value)
    {
        return $this->handle->sContains($this->pre . $key, $value);
    }

    /**
     * [sSize 返回集合中元素个数]
     * @author limx
     * @param $key
     */
    public function sSize($key)
    {
        return $this->handle->sSize($this->pre . $key);
    }

    /**
     * [sMembers 返回集合key中所有的元素]
     * @author limx
     * @param $key
     * @return array
     */
    public function sMembers($key)
    {
        return $this->handle->sMembers($this->pre . $key);
    }

    //SET操作相关 E

    //HASH操作相关 S
    public function hSet($key, $field, $value)
    {
        return $this->handle->hSet($this->pre . $key, $field, $value);
    }

    /**
     * [hMset desc]
     * @author limx
     * @param $key
     * @param $arr 数组['field'=>'value']
     * @return bool
     */
    public function hMset($key, $arr)
    {
        return $this->handle->hMset($this->pre . $key, $arr);
    }

    public function hGet($key, $field)
    {
        return $this->handle->hGet($this->pre . $key, $field);
    }

    public function hGetAll($key)
    {
        return $this->handle->hGetAll($this->pre . $key);
    }

    /**
     * [hDel desc]
     * @author limx
     * @param $key
     * @param $field string 删除一个值 array 删除一片
     * @return int
     */
    public function hDel($key, $field)
    {
        if (is_array($field)) {
            $count = 0;
            foreach ($field as $i => $v) {
                $count = $count + $this->handle->hDel($this->pre . $key, $v);
            }
            return $count;
        }
        return $this->handle->hDel($this->pre . $key, $field);
    }

    public function hLen($key)
    {
        return $this->handle->hLen($this->pre . $key);
    }

    public function hExists($key, $field)
    {
        return $this->handle->hExists($this->pre . $key, $field);
    }

    public function hKeys($key)
    {
        return $this->handle->hKeys($this->pre . $key);
    }

    public function hVals($key)
    {
        return $this->handle->hVals($this->pre . $key);
    }
    //HASH操作相关 E

    //List操作相关 S
    /**
     * [lPush 插入到列表头]
     * @author limx
     * @param $key 列表KEY
     * @param $value string 直接插入 []循环插入
     * @return bool|int
     */
    public function lPush($key, $value)
    {
        if (is_array($value)) {
            foreach ($value as $i => $v) {
                $this->handle->lPush($this->pre . $key, $v);
            }
            return true;
        }
        return $this->handle->lPush($this->pre . $key, $value);
    }

    /**
     * [rPush 插入到列表尾]
     * @author limx
     * @param $key
     * @param $value
     * @return bool|int
     */
    public function rPush($key, $value)
    {
        if (is_array($value)) {
            foreach ($value as $i => $v) {
                $this->handle->rPush($this->pre . $key, $v);
            }
            return true;
        }
        return $this->handle->rPush($this->pre . $key, $value);
    }

    /**
     * [lPop 弹出列表头]
     * @author limx
     * @param $key
     * @return string
     */
    public function lPop($key)
    {
        return $this->handle->lPop($this->pre . $key);
    }

    /**
     * [rPop 弹出列表尾]
     * @author limx
     * @param $key
     * @return string
     */
    public function rPop($key)
    {
        return $this->handle->rPop($this->pre . $key);
    }

    public function lLen($key)
    {
        return $this->handle->lLen($this->pre . $key);
    }

    public function lRange($key, $start, $end)
    {
        return $this->handle->lRange($this->pre . $key, $start, $end);
    }

    /**
     * [lRem 根据参数count的值，移除列表中与参数value相等的元素。]
     * @author limx
     * @param $key
     * @param $value
     * @param $count 0:全部
     * @return int
     */
    public function lRem($key, $value, $count)
    {
        return $this->handle->lRem($this->pre . $key, $value, $count);
    }

    /**
     * [lSet 将列表key下标为index的元素的值更改为value。]
     * @author limx
     * @param $key
     * @param $index
     * @param $value
     * @return BOOL
     */
    public function lSet($key, $index, $value)
    {
        return $this->handle->lSet($this->pre . $key, $index, $value);
    }

    //List操作相关 E


    //魔术方法
    public function __call($name, $arguments)
    {
        if (is_array($arguments)) {
            if (count($arguments) > 0) {
                $arguments[0] = $this->pre . $arguments[0];
            }
        }
        //$argument = implode(',', $arguments);
        //$this->handle->$name($argument);
        return call_user_func_array([$this->handle, $name], $arguments);
    }

}